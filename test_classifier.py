import numpy as np
import classifier


class TestClassifier:

    def test_prediction(self):
        # Points "prototypes"
        assert classifier.predict([np.zeros(30)]) in [0, 1]
